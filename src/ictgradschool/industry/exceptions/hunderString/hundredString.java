package ictgradschool.industry.exceptions.hunderString;

import java.util.Scanner;

public class hundredString {

    public String getUserInput() throws InvalidWordException, ExceedMaxStringLengthException {
        System.out.print("Enter a string of at most 100 characters: ");
        Scanner input = new Scanner(System.in);
        String input2 = input.nextLine();
        String[] words = input2.split(" ");
        for (String each : words) {
            if (Character.isDigit(each.charAt(0))) {
                throw new InvalidWordException();
            }
        }


        if (input2.length() > 100) {
            throw new ExceedMaxStringLengthException();
        }

        return input2;


    }

    public void start() {
        try {
            String x = getUserInput();
            String[] words = x.split(" ");
            for (String elements : words) {
                System.out.print(elements.charAt(0) + " ");
            }

        } catch (InvalidWordException e) {
            System.out.println("Invalid");
        } catch (ExceedMaxStringLengthException e) {
            System.out.println("Too long");
        }

    }

    public static void main(String args[]) {
        new hundredString().start();
    }


}

class InvalidWordException extends Exception {
}

class ExceedMaxStringLengthException extends Exception {
}